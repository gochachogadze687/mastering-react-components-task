import React, { Fragment } from 'react';
import Header from './Header';
import Main from './Main';
import Footer from './Footer';
import './App.css';

function App({ children }) {
    return (
       <div className="App">
      <Fragment>
        <Header />
        <Main />
        <Footer />
        {children}
      </Fragment>
      </div>
    );
  }

export default App;