import React from 'react';
import PropTypes from 'prop-types';
import styles from './Footer.module.css'; 

function Footer({ title }) {
  return <footer className={styles.footerComponent}>{title}</footer>; 
}

Footer.propTypes = {
  title: PropTypes.string,
};

Footer.defaultProps = {
  title: 'Mastering React Component',
};

export default Footer;