import React from 'react';
import PropTypes from 'prop-types';
import styles from './Header.module.css'; 

function Header({ title }) {
  return <header className={styles.headerComponent}>{title}</header>; 
}

Header.propTypes = {
  title: PropTypes.string,
};

Header.defaultProps = {
  title: 'Mastering React Component',
};

export default Header;