import React from 'react';
import PropTypes from 'prop-types';
import './App.css';

function Photos({ photoData }) {
  const photosToDisplay = photoData.slice(0, 10).map(photo => (
    <div key={photo.id}>
      <img src={photo.url} alt={photo.title} />
      <h3>{photo.title}</h3>
    </div>
  ));

  return <div className="photos">{photosToDisplay}</div>;
}

Photos.propTypes = {
  photoData: PropTypes.array.isRequired,
};

export default Photos;